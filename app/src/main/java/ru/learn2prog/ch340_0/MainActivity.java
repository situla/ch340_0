package ru.learn2prog.ch340_0;

import static java.lang.Thread.sleep;

import ru.learn2prog.ch340_0.ui.rlc.RLCFragment;
import ru.learn2prog.ch340_0.ui.rlc.RLCFragment.onRLCEventListener;
import ru.learn2prog.ch340_0.ui.rlc.RLCFragment.onRLCRadioButtonEventListener;
import ru.learn2prog.ch340_0.ui.rlc.RLCFragment.onRLCSetRelativeEventListener;
import ru.learn2prog.ch340_0.ui.rlc.RLCFragment.onRLCEqSChoiceEventListener;
import ru.learn2prog.ch340_0.ui.gallery.GalleryFragment.onUfdEventListener;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.hoho.android.usbserial.BuildConfig;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.StringTokenizer;

import ru.learn2prog.ch340_0.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements onRLCEventListener,
        onRLCRadioButtonEventListener, onRLCSetRelativeEventListener,
        onRLCEqSChoiceEventListener, RLCFragment.onRLCFragmentDetachListener,
        onUfdEventListener {

    private AppBarConfiguration mAppBarConfiguration;
    private ActivityMainBinding binding;
    private static final String INTENT_ACTION_GRANT_USB = BuildConfig.LIBRARY_PACKAGE_NAME + ".GRANT_USB";

    private boolean isRLCRunning = false;
    //private boolean modeIsChanged = false; //delete candidate
    private boolean isUfdRunning = false;

    private enum MODE {NONE, RLC_METER, U_F_DIODE, GENERATOR};
    private  MODE mode = MODE.NONE;

    static final int DEBUG_MODE = 1;

    UsbManager manager;
    UsbSerialDriver driver;
    UsbDeviceConnection connection;
    UsbSerialPort port;

    static TextView t, t_ufd;
    static TextView t_cl_95hz, t_r_95hz, t_z_95hz, t_qtg_95hz, t_eqs_95hz;
    static TextView t_cl_1khz, t_r_1khz, t_z_1khz, t_qtg_1khz, t_eqs_1khz;
    static TextView t_cl_10khz, t_r_10khz, t_z_10khz, t_qtg_10khz, t_eqs_10khz;
    static TextView t_cl_95khz, t_r_95khz, t_z_95khz, t_qtg_95khz, t_eqs_95khz;
    static TextView t_cl_160khz, t_r_160khz, t_z_160khz, t_qtg_160khz, t_eqs_160khz;
    static  TextView t_ufd_f, t_ufd_t, t_ufd_d, t_ufd_n, t_ufd_rms, t_ufd_ave, t_ufd_upp;

    static SwitchCompat swSetRelative;

    static Button RLCButton, UfdButton;
    //хендлеры для передачи сообщений в GUI
    Handler h;
    Handler h_ufd;

    @Override
    public void rlcFrDetachEvent() {
        //Log.d(">>Debug<<", "rlcFrDetachEvent: from MainActivity");
        isRLCRunning = false;
        if (RLCButton != null)  RLCButton.setText(R.string.rlc_start_button);
        mode = MODE.NONE;
    }

    @Override
    public void rlcEvent(String s) {
        // there is not such code "if (mode == MODE.RLC_METER)" here
        // because this is the first thing the RLC-mode begins to work with
        if (isRLCRunning) {
            isRLCRunning = false;
            if (RLCButton != null) RLCButton.setText(R.string.rlc_start_button);
        }
        else {
            if (connection != null) {
                mode = MODE.RLC_METER;
                //modeIsChanged = true;

                t = (TextView) findViewById(R.id.text_debug);
                //change fonts style
                setRLCTableFonts(s);

                RLCButton = (Button) findViewById(R.id.button);
                RLCButton.setText(R.string.rlc_stop_button);

                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, "Mode  " + s + " started!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                //t.setText("Mode  " + s + " started!");
                // again hardcoded string:
                Resources res = getResources();
                String text = String.format(res.getString(R.string.mode_x_started), s);
                t.setText(text);

                isRLCRunning = true;
                startRLCMeter(s);
            }
        }
    }

    @Override
    public void UfdEvent(String s) {
        if (isUfdRunning) {
            isUfdRunning = false;
            if (UfdButton != null) UfdButton.setText(R.string.ufd_start_button);
        }
        else {
            if (connection != null) {
                mode = MODE.U_F_DIODE;
                //modeIsChanged = true;

                t_ufd = (TextView) findViewById(R.id.text_ufd_debug);
                t_ufd_f = (TextView) findViewById(R.id.text_ufd_f);
                t_ufd_t = (TextView) findViewById(R.id.text_ufd_t);
                t_ufd_d = (TextView) findViewById(R.id.text_ufd_d);
                t_ufd_n = (TextView) findViewById(R.id.text_ufd_n);
                t_ufd_rms = (TextView) findViewById(R.id.text_ufd_rms);
                t_ufd_ave = (TextView) findViewById(R.id.text_ufd_ave);
                t_ufd_upp = (TextView) findViewById(R.id.text_ufd_upp);

                UfdButton = (Button) findViewById(R.id.button);
                UfdButton.setText(R.string.ufd_stop_button);

                View parentLayout = findViewById(android.R.id.content);
                Snackbar.make(parentLayout, "Mode  " + s + " started!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                //t_ufd.setText("Mode  " + s + " started!");
                // again hardcoded string:
                Resources res = getResources();
                String text = String.format(res.getString(R.string.mode_x_started), s);
                t_ufd.setText(text);

                isUfdRunning = true;
                startUfd(s);
            }
        }
    }

    @Override
    public void rlcRadioButtonEvent(int id) {
        if ( mode == MODE.RLC_METER) {
            t = (TextView) findViewById(R.id.text_debug);

            if (id == R.id.radio_auto) {
                if (isRLCRunning) {
                    isRLCRunning = false;
                    RLCButton.setText(R.string.rlc_stop_button);
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                if (connection != null) {
                    mode = MODE.RLC_METER;
                    //modeIsChanged = true;
                    isRLCRunning = true;
                    // command  fall
                    startRLCMeter("fall\r");
                    RLCButton.setText(R.string.rlc_stop_button);
                    t.setText(R.string.auto_mode_started);
                    setRLCTableFonts("fall\r");
                }

            } else if (id == R.id.radio_f95) {
                if (isRLCRunning) {
                    isRLCRunning = false;
                    RLCButton.setText(R.string.rlc_start_button);
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                if (connection != null) {
                    mode = MODE.RLC_METER;
                    //modeIsChanged = true;
                    isRLCRunning = true;
                    startRLCMeter("f95\r");
                    RLCButton.setText(R.string.rlc_stop_button);
                    t.setText(R.string.f95_mode_started);
                    setRLCTableFonts("f95\r");
                }

            } else if (id == R.id.radio_f1k) {
                if (isRLCRunning) {
                    isRLCRunning = false;
                    RLCButton.setText(R.string.rlc_start_button);
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                if (connection != null) {
                    mode = MODE.RLC_METER;
                    //modeIsChanged = true;
                    isRLCRunning = true;
                    startRLCMeter("f1k\r");
                    RLCButton.setText(R.string.rlc_stop_button);
                    t.setText(R.string.f1k_mode_started);
                    setRLCTableFonts("f1k\r");
                }

            } else if (id == R.id.radio_f10k) {
                if (isRLCRunning) {
                    isRLCRunning = false;
                    RLCButton.setText(R.string.rlc_start_button);
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                if (connection != null) {
                    mode = MODE.RLC_METER;
                    //modeIsChanged = true;
                    isRLCRunning = true;
                    startRLCMeter("f10k\r");
                    RLCButton.setText(R.string.rlc_stop_button);
                    t.setText(R.string.f10k_mode_started);
                    setRLCTableFonts("f10k\r");
                }


            } else if (id == R.id.radio_f95k) {
                if (isRLCRunning) {
                    isRLCRunning = false;
                    RLCButton.setText(R.string.rlc_start_button);
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                if (connection != null) {
                    mode = MODE.RLC_METER;
                    //modeIsChanged = true;
                    isRLCRunning = true;
                    startRLCMeter("f95k\r");
                    RLCButton.setText(R.string.rlc_stop_button);
                    t.setText(R.string.f95k_mode_started);
                    setRLCTableFonts("f95k\r");
                }


            } else if (id == R.id.radio_f160k) {
                if (isRLCRunning) {
                    isRLCRunning = false;
                    RLCButton.setText(R.string.rlc_start_button);
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                if (connection != null) {
                    mode = MODE.RLC_METER;
                    //modeIsChanged = true;
                    isRLCRunning = true;
                    startRLCMeter("f160k\r");
                    RLCButton.setText(R.string.rlc_stop_button);
                    t.setText(R.string.f160k_mode_started);
                    setRLCTableFonts("f160k\r");
                }


            }
        }
    }

    @Override
    public void rlcSetRelativeEvent(boolean isChecked) {
        if ( mode == MODE.RLC_METER) {

            t = (TextView) findViewById(R.id.text_debug);

            RLCButton = (Button) findViewById(R.id.button);
            if (isChecked) {
                if (isRLCRunning) {
                    isRLCRunning = false;
                    RLCButton.setText(R.string.rlc_start_button);
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                if (connection != null) {
                    mode = MODE.RLC_METER;
                    //modeIsChanged = true;
                    isRLCRunning = true;
                    startRLCMeter("rel\r");
                    RLCButton.setText(R.string.rlc_stop_button);
                    t.setText(R.string.set_relation_on);
                }

            } else {
                if (isRLCRunning) {
                    isRLCRunning = false;
                    RLCButton.setText(R.string.rlc_start_button);
                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                if (connection != null) {
                    mode = MODE.RLC_METER;
                    //modeIsChanged = true;
                    isRLCRunning = true;
                    startRLCMeter("abs\r");
                    RLCButton.setText(R.string.rlc_stop_button);
                    t.setText(R.string.set_relation_on);
                }

            }
        }
    }

    @Override
    public void rlcEqSChoiceEvent(String choice) {
        RLCButton = (Button) findViewById(R.id.button);
        if (choice.equals("Auto") && mode == MODE.RLC_METER)  {
            // здесь шляпа - в спиннере при старте активити происходит выбор "Auto",
            // поэтому введен режим MODE.NONE
            if (isRLCRunning) {
                isRLCRunning = false;
                RLCButton.setText(R.string.rlc_start_button);
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            if (connection != null) {
                mode = MODE.RLC_METER;
                //modeIsChanged = true;
                isRLCRunning = true;
                startRLCMeter("auto\r");
                RLCButton.setText(R.string.rlc_stop_button);
                t.setText(R.string.eqs_auto_mode_started);
            }

        }
        if (choice.equals("Par") && mode == MODE.RLC_METER) {
            if (isRLCRunning) {
                isRLCRunning = false;
                RLCButton.setText(R.string.rlc_start_button);
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            if (connection != null) {
                mode = MODE.RLC_METER;
                //modeIsChanged = true;
                isRLCRunning = true;
                startRLCMeter("par\r");
                RLCButton.setText(R.string.rlc_stop_button);
                t.setText(R.string.eqs_par_mode_started);
            }
        }
        if (choice.equals("Ser") && mode == MODE.RLC_METER) {
            if (isRLCRunning) {
                isRLCRunning = false;
                RLCButton.setText(R.string.rlc_start_button);
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }

            if (connection != null) {
                mode = MODE.RLC_METER;
                //modeIsChanged = true;
                isRLCRunning = true;
                startRLCMeter("ser\r");
                RLCButton.setText(R.string.rlc_stop_button);
                t.setText(R.string.eqs_ser_mode_started);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRLCRunning = false;
        if (RLCButton != null)  RLCButton.setText(R.string.rlc_start_button);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mode = MODE.NONE;

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        View parentLayout = findViewById(android.R.id.content);

        setSupportActionBar(binding.appBarMain.toolbar);
        binding.appBarMain.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "My own action", Snackbar.LENGTH_LONG)
                //        .setAction("Action", null).show();

                // try read and write
//                if ( connection != null) {
//                    try {
//                        port.write("help\r".getBytes(), 2000);
//                    } catch (IOException e) {
//                        throw new RuntimeException(e);
//                    }
//                    byte[] buffer = new byte[256];
//                    try {
//                        int len = port.read(buffer, 2000);
//                    } catch (IOException e) {
//                        throw new RuntimeException(e);
//                    }
//                    String response = new String(buffer);
//                    //Snackbar.make(view, response, Snackbar.LENGTH_LONG)
//                    //        .setAction("Action", null).show();
//
//                    int start_help_message  = response.indexOf("BM");
//
//                    //t.setText( response + ": " + response.indexOf("\n") + ": " + response.indexOf("\n", 1) + ": " + response.lastIndexOf("\n"));
//                    t.setText(response.substring(start_help_message, 11));
//                }
                if (t != null) t.setText(R.string.help_string);
                if (t_ufd != null) t_ufd.setText(R.string.help_string);
            }
        });

        DrawerLayout drawer = binding.drawerLayout;
        NavigationView navigationView = binding.navView;
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        t = (TextView) findViewById(R.id.text_debug);
        t_ufd = (TextView) findViewById(R.id.text_ufd_debug);

        // Find all available drivers from attached devices.
        manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(manager);
        if (availableDrivers.isEmpty()) {
            Snackbar.make(parentLayout, "availableDrivers List is Empty", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            // not work here
            t.setText(R.string.available_drivers_list_empty);
            return;
        } else {
            //Snackbar.make(view, "availableDrivers List is NOT Empty", Snackbar.LENGTH_LONG)
            // .setAction("Action", null).show();
        }

        // Open a connection to the first available driver.
        driver = availableDrivers.get(0);
        connection = manager.openDevice(driver.getDevice());
        if (connection == null) {
            // add UsbManager.requestPermission(driver.getDevice(), ..) handling here
            PendingIntent usbPermissionIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(INTENT_ACTION_GRANT_USB), PendingIntent.FLAG_IMMUTABLE);
            manager.requestPermission(driver.getDevice(), usbPermissionIntent);
            Snackbar.make(parentLayout, "UsbDeviceConnection is Null, reqPermission start!", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            //return;
        } else {
            Snackbar.make(parentLayout, "Tweezers is connected", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            // not work here
            t.setText(R.string.tweezers_is_connected);
        }

        port = driver.getPorts().get(0); // Most devices have just one port (port 0)
        try {
            port.open(connection);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        try {
            port.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        byte[] buffer = new byte[256];
        // command
        try {
            port.write("help\r".getBytes(), 200);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            int len = port.read(buffer, 2000);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        String response = new String(buffer, 0, buffer.length);
        t.setText(response);

//        RadioButton autoRadioButton = findViewById(R.id.radio_auto);
//        autoRadioButton.setOnClickListener(radioButtonClickListener);
//
//        RadioButton f95RadioButton = findViewById(R.id.radio_f95);
//        f95RadioButton.setOnClickListener(radioButtonClickListener);

        // thread messages handlers
        h = new MyHandler(this);
        h_ufd = new MyUfdHandler(this);
        //
        isRLCRunning = false;
        isUfdRunning =false;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void startRLCMeter(String cmd) {

        // start a new thread to ...
        Thread rlc_meter_thr = new Thread() {

            public void run() {
                // set process priority
                setPriority(Thread.MAX_PRIORITY);

                byte[] buffer = new byte[90];
                // command rlc
                try {
                    //port.write("rlc\r".getBytes(), 200);
                    port.write(cmd.getBytes(), 200);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                while(isRLCRunning) {
                    // read data
                    try {
                        int len = port.read(buffer, 2000);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    String response = new String(buffer, 0, buffer.length);
                    //Snackbar.make(view, response, Snackbar.LENGTH_LONG)
                    //        .setAction("Action", null).show();

//                    // java.lang.UnsupportedOperationException in this block
//                    try {
//                        port.purgeHwBuffers(false, true);
//                    } catch (IOException e) {
//                        throw new RuntimeException(e);
//                    }

                    Message msg = Message.obtain(); // Creates an new Message instance
                    msg.obj = response; // Put the string into Message, into "obj" field.
                    msg.setTarget(h); // Set the Handler
                    msg.sendToTarget(); //Send the message

//                    try {
//                        sleep(20);
//                    } catch (InterruptedException e) {
//                        throw new RuntimeException(e);
//                    }

                }

            }
        };
        rlc_meter_thr.start();
    }

    public void startUfd(String cmd) {

        // start a new thread to ...
        Thread ufd_thr = new Thread() {

            public void run() {
                // set process priority
                setPriority(Thread.MAX_PRIORITY);
                byte[] buffer = new byte[190];

                if (1>0) {
                    //byte[] buffer = new byte[190];
                    // command ufd
                    try {
                        port.write(cmd.getBytes(), 200);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }

                }

                while(isUfdRunning) {

                    // read data
                    try {
                        int len = port.read(buffer, 2000);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                    String response = new String(buffer, 0, buffer.length);

                    Message msg = Message.obtain(); // Creates an new Message instance
                    msg.obj = response; // Put the string into Message, into "obj" field.
                    msg.setTarget(h_ufd); // Set the Handler
                    msg.sendToTarget(); //Send the message

                    try {
                        sleep(500);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }

                }

            }
        };
        ufd_thr.start();
    }

    public void stopRLCMeter() {
        //btnSt.setEnabled(true);
        isRLCRunning = false;

    }

    // RLC data flow messages handler
    static class MyHandler extends Handler {

        WeakReference<MainActivity> wrActivity;

        public MyHandler(MainActivity activity) {
            wrActivity = new WeakReference<MainActivity>(activity);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            MainActivity activity = wrActivity.get();
            if (activity != null) {
                // обновляем отладочный TextView
                if (DEBUG_MODE == 1) t.setText(msg.obj.toString());

                StringTokenizer st;
                st = new StringTokenizer(msg.obj.toString(), ",");
                String freq_str = "";
                if ( st.hasMoreTokens() ) {
                    freq_str = (st.nextToken()).trim();
                }

                if ( freq_str.equals("F=95Hz") ) {
                    if ( st.hasMoreTokens() ) t_cl_95hz.setText( (st.nextToken()).trim().replace("L=", "").replace("C=", "") );
                    if ( st.hasMoreTokens() ) t_r_95hz.setText( replaceOhm ((st.nextToken()).trim().replace("R=", "")) );
                    if ( st.hasMoreTokens() ) t_eqs_95hz.setText( (st.nextToken()).trim() ); // Eq S
                    if ( st.hasMoreTokens() ) t_z_95hz.setText( replaceOhm ((st.nextToken()).trim().replace("Z=", "")) );
                    if ( st.hasMoreTokens() ) t_qtg_95hz.setText( (st.nextToken()).trim().replace("Q=", "").replace("tg=", "") );
                }
                if ( freq_str.equals("F=1kHz") ) {
                    if ( st.hasMoreTokens() ) t_cl_1khz.setText( (st.nextToken()).trim().replace("L=", "").replace("C=", "") );
                    if ( st.hasMoreTokens() ) t_r_1khz.setText( replaceOhm ((st.nextToken()).trim().replace("R=", "")) );
                    if ( st.hasMoreTokens() ) t_eqs_1khz.setText( (st.nextToken()).trim() ); // Eq S
                    if ( st.hasMoreTokens() ) t_z_1khz.setText( replaceOhm ((st.nextToken()).trim().replace("Z=", "")) );
                    if ( st.hasMoreTokens() ) t_qtg_1khz.setText( (st.nextToken()).trim().replace("Q=", "").replace("tg=", "") );
                }
                if ( freq_str.equals("F=10kHz") ) {
                    if ( st.hasMoreTokens() ) t_cl_10khz.setText( (st.nextToken()).trim().replace("L=", "").replace("C=", "") );
                    if ( st.hasMoreTokens() ) t_r_10khz.setText( replaceOhm ((st.nextToken()).trim().replace("R=", "")) );
                    if ( st.hasMoreTokens() ) t_eqs_10khz.setText( (st.nextToken()).trim() ); // Eq S
                    if ( st.hasMoreTokens() ) t_z_10khz.setText( replaceOhm ((st.nextToken()).trim().replace("Z=", "")) );
                    if ( st.hasMoreTokens() ) t_qtg_10khz.setText( (st.nextToken()).trim().replace("Q=", "").replace("tg=", "") );
                }
                if ( freq_str.equals("F=95kHz") ) {
                    if ( st.hasMoreTokens() ) t_cl_95khz.setText( (st.nextToken()).trim().replace("L=", "").replace("C=", "") );
                    if ( st.hasMoreTokens() ) t_r_95khz.setText( replaceOhm ((st.nextToken()).trim().replace("R=", "")) );
                    if ( st.hasMoreTokens() ) t_eqs_95khz.setText( (st.nextToken()).trim() ); // Eq S
                    if ( st.hasMoreTokens() ) t_z_95khz.setText( replaceOhm ((st.nextToken()).trim().replace("Z=", "")) );
                    if ( st.hasMoreTokens() ) t_qtg_95khz.setText( (st.nextToken()).trim().replace("Q=", "").replace("tg=", "") );
                }
                if ( freq_str.equals("F=160kHz") ) {
                    if ( st.hasMoreTokens() ) t_cl_160khz.setText( (st.nextToken()).trim().replace("L=", "").replace("C=", "") );
                    if ( st.hasMoreTokens() ) t_r_160khz.setText( replaceOhm ((st.nextToken()).trim().replace("R=", "")) );
                    if ( st.hasMoreTokens() ) t_eqs_160khz.setText( (st.nextToken()).trim() ); // Eq S
                    if ( st.hasMoreTokens() ) t_z_160khz.setText( replaceOhm ((st.nextToken()).trim().replace("Z=", "")) );
                    if ( st.hasMoreTokens() ) t_qtg_160khz.setText( (st.nextToken()).trim().replace("Q=", "").replace("tg=", "") );
                }

            }

        }
    }

    // Ufd data flow messages handler
    static class MyUfdHandler extends Handler {

        WeakReference<MainActivity> wrActivity;

        public MyUfdHandler(MainActivity activity) {
            wrActivity = new WeakReference<MainActivity>(activity);
        }

        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            MainActivity activity = wrActivity.get();
            if (activity != null) {
                // debug TextView
                if (DEBUG_MODE == 1) t_ufd.setText(msg.obj.toString());

                String str = msg.obj.toString();
                int l_ind = -1;
                int f_ind = str.indexOf("f=");
                if ( f_ind >= 0) l_ind = str.substring(f_ind).indexOf("Hz");
                if ( f_ind >= 0 && l_ind >= 0 ) t_ufd_f.setText(str.substring(f_ind, f_ind + l_ind + 2));

                f_ind = str.indexOf("T=");
                if ( f_ind >= 0) l_ind = str.substring(f_ind).indexOf("s"); else l_ind = -1;
                if ( f_ind >= 0 && l_ind >= 0 ) t_ufd_t.setText(str.substring(f_ind, f_ind + l_ind + 1));

                f_ind = str.indexOf("D=");
                if ( f_ind >= 0 ) t_ufd_d.setText(str.substring(f_ind, f_ind + 6));

                f_ind = str.indexOf("N=");
                if ( f_ind >= 0 ) t_ufd_d.setText(str.substring(f_ind, f_ind + 9));

                f_ind = str.indexOf("Uave");
                if ( f_ind >= 0) l_ind = str.substring(f_ind).indexOf("V"); else l_ind = -1;
                if ( f_ind >= 0 && l_ind >= 0 ) t_ufd_ave.setText(str.substring(f_ind, f_ind + l_ind + 1));

                f_ind = str.indexOf("Urms");
                if ( f_ind >= 0) l_ind = str.substring(f_ind).indexOf("V"); else l_ind = -1;
                if ( f_ind >= 0 && l_ind >= 0 ) t_ufd_rms.setText(str.substring(f_ind, f_ind + l_ind + 1));

                f_ind = str.indexOf("Up-p");
                if ( f_ind >= 0) l_ind = str.substring(f_ind).indexOf("V"); else l_ind = -1;
                if ( f_ind >= 0 && l_ind >= 0 ) t_ufd_upp.setText(str.substring(f_ind, f_ind + l_ind + 1));

            }

        }
    }

    static String replaceOhm(String str) {
        String tmp;
        // delete last odd symbol and add "Ω"
        tmp = str.substring(0, str.length() - 1) + "Ω";
        return tmp;
    }

    private void setRLCTableFonts(String s) {

        t_cl_95hz = (TextView) findViewById(R.id.TextCL95Hz);
        t_cl_95hz.setTypeface(null, Typeface.ITALIC);
        t_r_95hz = (TextView) findViewById(R.id.TextR95Hz);
        t_r_95hz.setTypeface(null, Typeface.ITALIC);
        t_z_95hz = (TextView) findViewById(R.id.TextZ95Hz);
        t_z_95hz.setTypeface(null, Typeface.ITALIC);
        t_qtg_95hz = (TextView) findViewById(R.id.TextQtg95Hz);
        t_qtg_95hz.setTypeface(null, Typeface.ITALIC);
        t_eqs_95hz = (TextView) findViewById(R.id.TextEqS95Hz);
        t_eqs_95hz.setTypeface(null, Typeface.ITALIC);
        t_cl_1khz = (TextView) findViewById(R.id.TextCL1kHz);
        t_cl_1khz.setTypeface(null, Typeface.ITALIC);
        t_r_1khz = (TextView) findViewById(R.id.TextR1kHz);
        t_r_1khz.setTypeface(null, Typeface.ITALIC);
        t_z_1khz = (TextView) findViewById(R.id.TextZ1kHz);
        t_z_1khz.setTypeface(null, Typeface.ITALIC);
        t_qtg_1khz = (TextView) findViewById(R.id.TextQtg1kHz);
        t_qtg_1khz.setTypeface(null, Typeface.ITALIC);
        t_eqs_1khz = (TextView) findViewById(R.id.TextEqS1kHz);
        t_eqs_1khz.setTypeface(null, Typeface.ITALIC);
        t_cl_10khz = (TextView) findViewById(R.id.TextCL10kHz);
        t_cl_10khz.setTypeface(null, Typeface.ITALIC);
        t_r_10khz = (TextView) findViewById(R.id.TextR10kHz);
        t_r_10khz.setTypeface(null, Typeface.ITALIC);
        t_z_10khz = (TextView) findViewById(R.id.TextZ10kHz);
        t_z_10khz.setTypeface(null, Typeface.ITALIC);
        t_qtg_10khz = (TextView) findViewById(R.id.TextQtg10kHz);
        t_qtg_10khz.setTypeface(null, Typeface.ITALIC);
        t_eqs_10khz = (TextView) findViewById(R.id.TextEqS10kHz);
        t_eqs_10khz.setTypeface(null, Typeface.ITALIC);
        t_cl_95khz = (TextView) findViewById(R.id.TextCL95kHz);
        t_cl_95khz.setTypeface(null, Typeface.ITALIC);
        t_r_95khz = (TextView) findViewById(R.id.TextR95kHz);
        t_r_95khz.setTypeface(null, Typeface.ITALIC);
        t_z_95khz = (TextView) findViewById(R.id.TextZ95kHz);
        t_z_95khz.setTypeface(null, Typeface.ITALIC);
        t_qtg_95khz = (TextView) findViewById(R.id.TextQtg95kHz);
        t_qtg_95khz.setTypeface(null, Typeface.ITALIC);
        t_eqs_95khz = (TextView) findViewById(R.id.TextEqS95kHz);
        t_eqs_95khz.setTypeface(null, Typeface.ITALIC);
        t_cl_160khz = (TextView) findViewById(R.id.TextCL160kHz);
        t_cl_160khz.setTypeface(null, Typeface.ITALIC);
        t_r_160khz = (TextView) findViewById(R.id.TextR160kHz);
        t_r_160khz.setTypeface(null, Typeface.ITALIC);
        t_z_160khz = (TextView) findViewById(R.id.TextZ160kHz);
        t_z_160khz.setTypeface(null, Typeface.ITALIC);
        t_qtg_160khz = (TextView) findViewById(R.id.TextQtg160kHz);
        t_qtg_160khz.setTypeface(null, Typeface.ITALIC);
        t_eqs_160khz = (TextView) findViewById(R.id.TextEqS160kHz);
        t_eqs_160khz.setTypeface(null, Typeface.ITALIC);

        if ( s.equals("f95\r")) {
            t_cl_95hz.setTypeface(null, Typeface.BOLD);
            t_r_95hz.setTypeface(null, Typeface.BOLD);
            t_eqs_95hz.setTypeface(null, Typeface.BOLD);
            t_z_95hz.setTypeface(null, Typeface.BOLD);
            t_qtg_95hz.setTypeface(null, Typeface.BOLD);
        }
        if ( s.equals("f1k\r")) {
            t_cl_1khz.setTypeface(null, Typeface.BOLD);
            t_r_1khz.setTypeface(null, Typeface.BOLD);
            t_eqs_1khz.setTypeface(null, Typeface.BOLD);
            t_z_1khz.setTypeface(null, Typeface.BOLD);
            t_qtg_1khz.setTypeface(null, Typeface.BOLD);
        }
        if ( s.equals("f10k\r")) {
            t_cl_10khz.setTypeface(null, Typeface.BOLD);
            t_r_10khz.setTypeface(null, Typeface.BOLD);
            t_eqs_10khz.setTypeface(null, Typeface.BOLD);
            t_z_10khz.setTypeface(null, Typeface.BOLD);
            t_qtg_10khz.setTypeface(null, Typeface.BOLD);
        }
        if ( s.equals("f95k\r")) {
            t_cl_95khz.setTypeface(null, Typeface.BOLD);
            t_r_95khz.setTypeface(null, Typeface.BOLD);
            t_eqs_95khz.setTypeface(null, Typeface.BOLD);
            t_z_95khz.setTypeface(null, Typeface.BOLD);
            t_qtg_95khz.setTypeface(null, Typeface.BOLD);
        }
        if ( s.equals("f160k\r")) {
            t_cl_160khz.setTypeface(null, Typeface.BOLD);
            t_r_160khz.setTypeface(null, Typeface.BOLD);
            t_eqs_160khz.setTypeface(null, Typeface.BOLD);
            t_z_160khz.setTypeface(null, Typeface.BOLD);
            t_qtg_160khz.setTypeface(null, Typeface.BOLD);
        }

    }

}