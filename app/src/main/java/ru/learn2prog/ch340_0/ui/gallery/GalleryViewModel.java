package ru.learn2prog.ch340_0.ui.gallery;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GalleryViewModel extends ViewModel {

    private final MutableLiveData<String> mText;
    private final MutableLiveData<String> buttonName;

    public GalleryViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Debug info");
        buttonName = new MutableLiveData<>();
        buttonName.setValue("Start Ufd mode");
    }

    public LiveData<String> getText() {
        return mText;
    }
    public LiveData<String> getButtonName() {
        return buttonName;
    }

}