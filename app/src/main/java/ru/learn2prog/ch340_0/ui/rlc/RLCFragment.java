package ru.learn2prog.ch340_0.ui.rlc;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import java.util.Objects;

import ru.learn2prog.ch340_0.R;
import ru.learn2prog.ch340_0.databinding.FragmentRlcBinding;

public class RLCFragment extends Fragment {

    public interface onRLCEventListener {
        public void rlcEvent(String s);
    }

    onRLCEventListener rlcEventListener;

    public interface onRLCRadioButtonEventListener {
        public void rlcRadioButtonEvent(int id);
    }

    onRLCRadioButtonEventListener rlcRadioButtonEventListener;

    public interface onRLCSetRelativeEventListener {
        public void rlcSetRelativeEvent(boolean isChecked);
    }

    onRLCSetRelativeEventListener rlcSetRelativeEventListener;

    public interface onRLCEqSChoiceEventListener {
        public void rlcEqSChoiceEvent(String choice);
    }

    onRLCEqSChoiceEventListener rlcEqSChoiceEventListener;

    public interface onRLCFragmentDetachListener {
        public void rlcFrDetachEvent();
    }

    onRLCFragmentDetachListener rlcFragmentDetachListener;

    @Override
    public void onAttach(@NonNull Activity activity) {
        //Log.d("<<Debg>>", "onAttach: hockey");
        super.onAttach(activity);
        try {
            rlcEventListener = (onRLCEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onRLCEventListener");
        }
        try {
            rlcRadioButtonEventListener = (onRLCRadioButtonEventListener) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString() + " must implement onRLCRadioButtonEventListener");
        }
        try {
            rlcSetRelativeEventListener = (onRLCSetRelativeEventListener) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString() + " must implement onRLCSetRelativeEventListener");
        }
        try {
            rlcEqSChoiceEventListener = (onRLCEqSChoiceEventListener) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString() + " must implement onRLCEqSChoiceEventListener");
        }
        try {
            rlcFragmentDetachListener = (onRLCFragmentDetachListener) activity;
        } catch (ClassCastException ex) {
            throw new ClassCastException(activity.toString() + " must implement onRLCFragmentDetachListener");
        }
    }

    @Override
    public void onResume() {
        //сначала делаем нужное
        //Log.d(">>Debug<<", "rlcFrDetachEvent: from Fragment");
        rlcFragmentDetachListener.rlcFrDetachEvent();
        //потом метод суперкласса
        super.onResume();
    }

    private FragmentRlcBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        RLCViewModel RLCViewModel =
                new ViewModelProvider(this).get(RLCViewModel.class);

        binding = FragmentRlcBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textDebug;
        RLCViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        final Button btn = binding.button;
        RLCViewModel.getButtonName().observe(getViewLifecycleOwner(), btn::setText);
        btn.setOnClickListener(jsv -> {
            //textView.setText("My new home");
            rlcEventListener.rlcEvent("rlc\r");
        });

        final RadioButton rbAuto = binding.radioAuto;
        rbAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlcRadioButtonEventListener.rlcRadioButtonEvent(rbAuto.getId());
            }
        });

        final RadioButton rbF95 = binding.radioF95;
        rbF95.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlcRadioButtonEventListener.rlcRadioButtonEvent(rbF95.getId());
            }
        });

        final RadioButton rbF1k = binding.radioF1k;
        rbF1k.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlcRadioButtonEventListener.rlcRadioButtonEvent(rbF1k.getId());
            }
        });

        final RadioButton rbF10k = binding.radioF10k;
        rbF10k.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlcRadioButtonEventListener.rlcRadioButtonEvent(rbF10k.getId());
            }
        });

        final RadioButton rbF95k = binding.radioF95k;
        rbF95k.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlcRadioButtonEventListener.rlcRadioButtonEvent(rbF95k.getId());
            }
        });

        final RadioButton rbF160k = binding.radioF160k;
        rbF160k.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlcRadioButtonEventListener.rlcRadioButtonEvent(rbF160k.getId());
            }
        });

        final SwitchCompat swRelative = binding.relativeSw;
        swRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlcSetRelativeEventListener.rlcSetRelativeEvent(swRelative.isChecked());
            }
        });

        final Spinner eqs_spinner = binding.eqsSpinner;
        String[] eqs_spinner_values = { "Auto", "Ser", "Par"};
        ArrayAdapter<String> adapter = new ArrayAdapter(requireContext(), R.layout.spinner_item, eqs_spinner_values);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eqs_spinner.setAdapter(adapter);
        eqs_spinner.setPrompt("Title");

        eqs_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                rlcEqSChoiceEventListener.rlcEqSChoiceEvent(eqs_spinner.getSelectedItem().toString());
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}