package ru.learn2prog.ch340_0.ui.gallery;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import ru.learn2prog.ch340_0.databinding.FragmentGalleryBinding;
import ru.learn2prog.ch340_0.ui.rlc.RLCFragment;

public class GalleryFragment extends Fragment {

    public interface onUfdEventListener {
        public void UfdEvent(String s);
    }

    GalleryFragment.onUfdEventListener ufdEventListener;

    @Override
    public void onAttach(@NonNull Activity activity) {

        super.onAttach(activity);
        try {
            ufdEventListener = (GalleryFragment.onUfdEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onUfdEventListener");
        }
    }

    private FragmentGalleryBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        GalleryViewModel galleryViewModel =
                new ViewModelProvider(this).get(GalleryViewModel.class);

        binding = FragmentGalleryBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textUfdDebug;
        galleryViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);

        final Button btn = binding.button;
        galleryViewModel.getButtonName().observe(getViewLifecycleOwner(), btn::setText);
        btn.setOnClickListener(jsv -> {
            //Log.d("BUTTONS", "User tapped the button");
            //textView.setText("My new gallery");
            ufdEventListener.UfdEvent("ufd\r");
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}